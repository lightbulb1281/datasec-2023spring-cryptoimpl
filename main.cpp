#include <string>
#include <ostream>
#include "src/sha-256.h"
#include "src/sha-512.h"
#include "src/sha3.h"
#include "src/aes.h"
#include "src/sm3.h"
#include "src/sm4.h"

int main(int argc, char** argv) {

  if (argc == 1) {

    sm4::debug();

    return 0;
  }

  std::string algorithm = argv[1];

  if (algorithm == "sha256") {
    std::ostringstream sstream;
    sstream << std::cin.rdbuf();
    std::string input = sstream.str();
    auto digest = sha256::digest((uint8_t*)input.c_str(), input.size());
    sha256::print_result(digest);
  } 

  else if (algorithm == "sha512") {
    std::ostringstream sstream;
    sstream << std::cin.rdbuf();
    std::string input = sstream.str();
    auto digest = sha512::digest((uint8_t*)input.c_str(), input.size());
    sha512::print_result(digest);
  } 

  else if (algorithm == "sha3-256") {
    std::ostringstream sstream;
    sstream << std::cin.rdbuf();
    std::string input = sstream.str();
    auto digest = sha3::sha3_256((uint8_t*)input.c_str(), input.size());
    sha3::print_result(digest);
  }

  else if (algorithm == "sha3-512") {
    std::ostringstream sstream;
    sstream << std::cin.rdbuf();
    std::string input = sstream.str();
    auto digest = sha3::sha3_512((uint8_t*)input.c_str(), input.size());
    sha3::print_result(digest);
  }

  else if (algorithm == "aes128-ecb-enc") {
    std::ostringstream sstream;
    sstream << std::cin.rdbuf();
    std::string total = sstream.str();
    auto digest = aes::aes128_encrypt_ecb(
      (uint8_t*)(total.c_str() + 128 / 8), (total.size() - 128/8), 
      (uint8_t*)(total.c_str()));
    aes::print_result(digest);
  }

  else if (algorithm == "aes128-ecb-dec") {
    std::ostringstream sstream;
    sstream << std::cin.rdbuf();
    std::string total = sstream.str();
    auto digest = aes::aes128_decrypt_ecb(
      (uint8_t*)(total.c_str() + 128 / 8), (total.size() - 128/8), 
      (uint8_t*)(total.c_str()));
    aes::print_result(digest);
  }

  else if (algorithm == "aes192-ecb-enc") {
    std::ostringstream sstream;
    sstream << std::cin.rdbuf();
    std::string total = sstream.str();
    auto digest = aes::aes192_encrypt_ecb(
      (uint8_t*)(total.c_str() + 192 / 8), (total.size() - 192 / 8), 
      (uint8_t*)(total.c_str()));
    aes::print_result(digest);
  }

  else if (algorithm == "aes192-ecb-dec") {
    std::ostringstream sstream;
    sstream << std::cin.rdbuf();
    std::string total = sstream.str();
    auto digest = aes::aes192_decrypt_ecb(
      (uint8_t*)(total.c_str() + 192 / 8), (total.size() - 192 / 8), 
      (uint8_t*)(total.c_str()));
    aes::print_result(digest);
  }

  else if (algorithm == "aes256-ecb-enc") {
    std::ostringstream sstream;
    sstream << std::cin.rdbuf();
    std::string total = sstream.str();
    auto digest = aes::aes256_encrypt_ecb(
      (uint8_t*)(total.c_str() + 256 / 8), (total.size() - 256 / 8), 
      (uint8_t*)(total.c_str()));
    aes::print_result(digest);
  }

  else if (algorithm == "aes256-ecb-dec") {
    std::ostringstream sstream;
    sstream << std::cin.rdbuf();
    std::string total = sstream.str();
    auto digest = aes::aes256_decrypt_ecb(
      (uint8_t*)(total.c_str() + 256 / 8), (total.size() - 256 / 8), 
      (uint8_t*)(total.c_str()));
    aes::print_result(digest);
  }

  else if (algorithm == "sm3") {
    std::ostringstream sstream;
    sstream << std::cin.rdbuf();
    std::string input = sstream.str();
    auto digest = sm3::digest((uint8_t*)input.c_str(), input.size());
    sm3::print_result(digest);
  }

  else if (algorithm == "sm4-ecb-enc") {
    std::ostringstream sstream;
    sstream << std::cin.rdbuf();
    std::string total = sstream.str();
    auto digest = sm4::encrypt_ecb(
      (uint8_t*)(total.c_str() + 128 / 8), (total.size() - 128/8), 
      (uint8_t*)(total.c_str()));
    aes::print_result(digest);
  }

  else if (algorithm == "sm4-ecb-dec") {
    std::ostringstream sstream;
    sstream << std::cin.rdbuf();
    std::string total = sstream.str();
    auto digest = sm4::decrypt_ecb(
      (uint8_t*)(total.c_str() + 128 / 8), (total.size() - 128/8), 
      (uint8_t*)(total.c_str()));
    aes::print_result(digest);
  }
  
  else {
    std::cout << "Unknown algorithm: " << algorithm << std::endl;
  }

  return 0;
  
}