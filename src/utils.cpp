#include "utils.h"

namespace util {
    void print_hex_uint32_array(const uint32_t* value, int size) {
    for (int i = 0; i < size; ++i) {
      if (i % 8 == 0) {
        std::cout << "[";
      }
      std::cout << std::setw(8) << std::setfill('0') << std::hex << value[i];
      if (i % 8 != 7) std::cout << " ";
      if (i % 8 == 7 || i == size - 1) {
        std::cout << "]" << std::endl;
      }
    }
  }

  void print_hex_uint64_array(const uint64_t* value, int size) {
    for (int i = 0; i < size; ++i) {
      if (i % 4 == 0) {
        std::cout << "[";
      }
      std::cout << std::setw(16) << std::setfill('0') << std::hex << value[i];
      if (i % 4 != 3) std::cout << " ";
      if (i % 4 == 3 || i == size - 1) {
        std::cout << "]" << std::endl;
      }
    }
  }

  void print_hex_uint8_array(const uint8_t* value, int size) {
    for (int i = 0; i < size; ++i) {
      if (i % 16 == 0) {
        std::cout << "[";
      }
      std::cout << std::setw(2) << std::setfill('0') << std::hex << (int)value[i];
      if (i % 16 != 15) std::cout << " ";
      if (i % 16 == 15 || i == size - 1) {
        std::cout << "]" << std::endl;
      }
    }
  }
}