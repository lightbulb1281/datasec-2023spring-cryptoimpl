#include "utils.h"
#include <vector>

namespace sm3 {
  
  std::vector<uint32_t> pad(const uint8_t* data, size_t len) {
    std::vector<uint32_t> padded(len / 4 + (len % 4 > 0));
    memset(padded.data(), 0, padded.size() * sizeof(uint32_t));
    // copy data to padded, using 4 bytes for one uint32
    for (size_t i = 0; i < len; ++i) {
      padded[i / 4] |= data[i] << (8 * (3 - i % 4));
    }
    // append 1
    if (len / 4 >= padded.size()) {
      padded.push_back(0);
    }
    padded[len / 4] |= 0x80 << (8 * (3 - len % 4));
    // pad zeros until remain 2 uint32s to be a multiple of 512 bits
    while (padded.size() % 16 != 14) {
      padded.push_back(0);
    }
    uint64_t length = len * 8;
    padded.push_back(length >> 32);
    padded.push_back(length % (0xffffffff));
    return padded;
  }

  const uint32_t IV[8] = {
    0x7380166f, 0x4914b2b9, 0x172442d7, 0xda8a0600,
    0xa96f30bc, 0x163138aa, 0xe38dee4d, 0xb0fb0e4e
  };

  inline uint32_t T(size_t k) {
    if (k >= 0 && k <= 15) {
      return 0x79cc4519;
    } else if (k >= 16 && k <= 63) {
      return 0x7a879d8a;
    }
    return 0;
  }

  inline uint32_t FF(uint32_t x, uint32_t y, uint32_t z, size_t k) {
    if (k >= 0 && k <= 15) {
      return x ^ y ^ z;
    } else if (k >= 16 && k <= 63) {
      return (x & y) | (x & z) | (y & z);
    }
    return 0;
  }

  inline uint32_t GG(uint32_t x, uint32_t y, uint32_t z, size_t k) {
    if (k >= 0 && k <= 15) {
      return x ^ y ^ z;
    } else if (k >= 16 && k <= 63) {
      return (x & y) | (~x & z);
    }
    return 0;
  }

  inline uint32_t rotl(uint32_t x, size_t n) {
    return (x << n) | (x >> (32 - n));
  }

  inline uint32_t P0(uint32_t x) {
    return x ^ rotl(x, 9) ^ rotl(x, 17);
  }

  inline uint32_t P1(uint32_t x) {
    return x ^ rotl(x, 15) ^ rotl(x, 23);
  }

  void compress(uint32_t* state, const uint32_t* block) {
    uint32_t W[68];
    uint32_t W1[64];
    uint32_t SS1, SS2, TT1, TT2;
    uint32_t A, B, C, D, E, F, G, H;
    for (size_t i = 0; i < 16; ++i) {
      W[i] = block[i];
    }
    for (size_t i = 16; i < 68; ++i) {
      W[i] = P1(W[i - 16] ^ W[i - 9] ^ rotl(W[i - 3], 15)) ^ rotl(W[i - 13], 7) ^ W[i - 6];
    }
    for (size_t i = 0; i < 64; ++i) {
      W1[i] = W[i] ^ W[i + 4];
    }
    A = state[0];
    B = state[1];
    C = state[2];
    D = state[3];
    E = state[4];
    F = state[5];
    G = state[6];
    H = state[7];
    for (size_t i = 0; i < 64; ++i) {
      SS1 = rotl((rotl(A, 12) + E + rotl(T(i), i)), 7);
      SS2 = SS1 ^ rotl(A, 12);
      TT1 = FF(A, B, C, i) + D + SS2 + W1[i];
      TT2 = GG(E, F, G, i) + H + SS1 + W[i];
      D = C;
      C = rotl(B, 9);
      B = A;
      A = TT1;
      H = G;
      G = rotl(F, 19);
      F = E;
      E = P0(TT2);
    }
    state[0] ^= A;
    state[1] ^= B;
    state[2] ^= C;
    state[3] ^= D;
    state[4] ^= E;
    state[5] ^= F;
    state[6] ^= G;
    state[7] ^= H;
  }

  std::vector<uint32_t> digest(const uint8_t* data, size_t len) {
    std::vector<uint32_t> padded = pad(data, len);
    std::vector<uint32_t> state(8);
    memcpy(state.data(), IV, 8 * sizeof(uint32_t));
    for (size_t i = 0; i < padded.size() / 16; ++i) {
      compress(state.data(), padded.data() + i * 16);
    }
    return state;
  }

  void print_result(const std::vector<uint32_t> digest) {
    // use putchar to put uint8_ts to stdout
    for (size_t i = 0; i < digest.size(); ++i) {
      for (size_t j = 0; j < 4; ++j) {
        putchar((digest[i] >> (8 * (4 - j - 1))) & 0xFF); // big endian
      }
    }
  }

}
