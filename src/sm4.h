#include "utils.h"
#include <vector>

using std::vector;

namespace sm4 {

  typedef uint32_t State[4];
  
  void print_state(const State& A) {
    for (size_t i = 0; i < 4; ++i) {
      if (i % 2 == 0) printf("<");
      // print little endian u8s 
      for (size_t j = 0; j < 4; ++j) {
        printf("%02x", (uint8_t)(A[i] >> (8 * j)));
        if (j != 3) printf(" ");
        else if (i % 2 == 0) printf(" ");
      }
      if (i % 2 == 1) printf(">\n");
    }
    printf(">\n");
  }

  static const uint8_t sbox[256] = {
    0xd6, 0x90, 0xe9, 0xfe, 0xcc, 0xe1, 0x3d, 0xb7, 0x16, 0xb6, 0x14, 0xc2, 0x28, 0xfb, 0x2c, 0x05,
    0x2b, 0x67, 0x9a, 0x76, 0x2a, 0xbe, 0x04, 0xc3, 0xaa, 0x44, 0x13, 0x26, 0x49, 0x86, 0x06, 0x99,
    0x9c, 0x42, 0x50, 0xf4, 0x91, 0xef, 0x98, 0x7a, 0x33, 0x54, 0x0b, 0x43, 0xed, 0xcf, 0xac, 0x62,
    0xe4, 0xb3, 0x1c, 0xa9, 0xc9, 0x08, 0xe8, 0x95, 0x80, 0xdf, 0x94, 0xfa, 0x75, 0x8f, 0x3f, 0xa6,
    0x47, 0x07, 0xa7, 0xfc, 0xf3, 0x73, 0x17, 0xba, 0x83, 0x59, 0x3c, 0x19, 0xe6, 0x85, 0x4f, 0xa8,
    0x68, 0x6b, 0x81, 0xb2, 0x71, 0x64, 0xda, 0x8b, 0xf8, 0xeb, 0x0f, 0x4b, 0x70, 0x56, 0x9d, 0x35,
    0x1e, 0x24, 0x0e, 0x5e, 0x63, 0x58, 0xd1, 0xa2, 0x25, 0x22, 0x7c, 0x3b, 0x01, 0x21, 0x78, 0x87,
    0xd4, 0x00, 0x46, 0x57, 0x9f, 0xd3, 0x27, 0x52, 0x4c, 0x36, 0x02, 0xe7, 0xa0, 0xc4, 0xc8, 0x9e,
    0xea, 0xbf, 0x8a, 0xd2, 0x40, 0xc7, 0x38, 0xb5, 0xa3, 0xf7, 0xf2, 0xce, 0xf9, 0x61, 0x15, 0xa1,
    0xe0, 0xae, 0x5d, 0xa4, 0x9b, 0x34, 0x1a, 0x55, 0xad, 0x93, 0x32, 0x30, 0xf5, 0x8c, 0xb1, 0xe3,
    0x1d, 0xf6, 0xe2, 0x2e, 0x82, 0x66, 0xca, 0x60, 0xc0, 0x29, 0x23, 0xab, 0x0d, 0x53, 0x4e, 0x6f,
    0xd5, 0xdb, 0x37, 0x45, 0xde, 0xfd, 0x8e, 0x2f, 0x03, 0xff, 0x6a, 0x72, 0x6d, 0x6c, 0x5b, 0x51,
    0x8d, 0x1b, 0xaf, 0x92, 0xbb, 0xdd, 0xbc, 0x7f, 0x11, 0xd9, 0x5c, 0x41, 0x1f, 0x10, 0x5a, 0xd8,
    0x0a, 0xc1, 0x31, 0x88, 0xa5, 0xcd, 0x7b, 0xbd, 0x2d, 0x74, 0xd0, 0x12, 0xb8, 0xe5, 0xb4, 0xb0,
    0x89, 0x69, 0x97, 0x4a, 0x0c, 0x96, 0x77, 0x7e, 0x65, 0xb9, 0xf1, 0x09, 0xc5, 0x6e, 0xc6, 0x84,
    0x18, 0xf0, 0x7d, 0xec, 0x3a, 0xdc, 0x4d, 0x20, 0x79, 0xee, 0x5f, 0x3e, 0xd7, 0xcb, 0x39, 0x48
  };

  static uint8_t inv_sbox[256] = {0};

  uint32_t tau(uint32_t x) {
    uint32_t y = 0;
    for (size_t i = 0; i < 4; ++i) {
      y |= ((uint32_t)sbox[(x >> (8 * i)) & 0xff]) << (8 * i);
    }
    return y;
  }

  uint32_t inv_tau(uint32_t x) {
    if (inv_sbox[0] == 0) { // initialize
      for (size_t i = 0; i < 256; ++i) {
        inv_sbox[sbox[i]] = i;
      }
    }
    uint32_t y = 0;
    for (size_t i = 0; i < 4; ++i) {
      y |= ((uint32_t)inv_sbox[(x >> (8 * i)) & 0xff]) << (8 * i);
    }
    return y;
  }

  uint32_t rotl(uint32_t x, size_t n) {
    return (x << n) | (x >> (32 - n));
  }

  uint32_t rotr(uint32_t x, size_t n) {
    return (x >> n) | (x << (32 - n));
  }

  uint32_t L(uint32_t x) {
    return x ^ rotl(x, 2) ^ rotl(x, 10) ^ rotl(x, 18) ^ rotl(x, 24);
  }

  uint32_t L_prime(uint32_t x) {
    return x ^ rotl(x, 13) ^ rotl(x, 23);
  }

  uint32_t T(uint32_t x) {
    return L(tau(x));
  }

  uint32_t T_prime(uint32_t x) {
    return L_prime(tau(x));
  }

  const uint32_t FK[4] = {0xa3b1bac6, 0x56aa3350, 0x677d9197, 0xb27022dc};
  const uint32_t CK[32] = {
    0x00070e15, 0x1c232a31, 0x383f464d, 0x545b6269, 0x70777e85, 0x8c939aa1, 0xa8afb6bd, 0xc4cbd2d9,
    0xe0e7eef5, 0xfc030a11, 0x181f262d, 0x343b4249, 0x50575e65, 0x6c737a81, 0x888f969d, 0xa4abb2b9,
    0xc0c7ced5, 0xdce3eaf1, 0xf8ff060d, 0x141b2229, 0x30373e45, 0x4c535a61, 0x686f767d, 0x848b9299,
    0xa0a7aeb5, 0xbcc3cad1, 0xd8dfe6ed, 0xf4fb0209, 0x10171e25, 0x2c333a41, 0x484f565d, 0x646b7279
  };


  vector<uint32_t> expand_key(uint32_t* key) {
    vector<uint32_t> keys;
    keys.push_back(key[0] ^ FK[0]);
    keys.push_back(key[1] ^ FK[1]);
    keys.push_back(key[2] ^ FK[2]);
    keys.push_back(key[3] ^ FK[3]);
    for (size_t i = 0; i < 32; ++i) {
      keys.push_back(keys[i] ^ T_prime(keys[i + 1] ^ keys[i + 2] ^ keys[i + 3] ^ CK[i]));
    }
    vector<uint32_t> ret; ret.reserve(32);
    for (size_t i = 0; i < 32; ++i) {
      ret.push_back(keys[i + 4]);
    }
    return ret;
  }

  void encrypt_block(uint32_t* plain, uint32_t* expanded_key, uint32_t* out) {
    vector<uint32_t> ret; for (size_t i = 0; i < 4; i++) ret.push_back(plain[i]);
    for (size_t i = 0; i < 32; ++i) {
      ret.push_back(ret[i] ^ T(ret[i + 1] ^ ret[i + 2] ^ ret[i + 3] ^ expanded_key[i]));
    }
    for (size_t i = 0; i < 4; ++i) {
      out[i] = ret[35 - i];
    }
  }

  vector<uint8_t> encrypt_ecb(uint8_t* plain, size_t len, uint8_t* key) {
    auto input = util::pkcs7_pad(plain, len, 16);
    // to big endian
    for (size_t i = 0; i < input.size(); i+=4) {
      uint32_t* input_u32 = (uint32_t*)(input.data() + i);
      *input_u32 = util::to_big_endian(*input_u32);
    }
    for (size_t i = 0; i < 16; i+=4) {
      uint32_t* key_u32 = (uint32_t*)(key + i);
      *key_u32 = util::to_big_endian(*key_u32);
    }
    vector<uint32_t> expanded_key = expand_key((uint32_t*)key);
    vector<uint8_t> ret; ret.reserve(len);
    for (size_t i = 0; i < input.size(); i += 16) {
      uint32_t tmp[4];
      encrypt_block((uint32_t*)(input.data() + i), expanded_key.data(), tmp);
      for (size_t j = 0; j < 16; ++j) {
        ret.push_back(((uint8_t*)tmp)[j]);
      }
    }
    // to little endian
    for (size_t i = 0; i < ret.size(); i+=4) {
      uint32_t* ret_u32 = (uint32_t*)(ret.data() + i);
      *ret_u32 = util::to_big_endian(*ret_u32);
    }
    return ret;
  }

  vector<uint8_t> decrypt_ecb(uint8_t* cipher, size_t len, uint8_t* key) {
    // to big endian
    for (size_t i = 0; i < len; i+=4) {
      uint32_t* cipher_u32 = (uint32_t*)(cipher + i);
      *cipher_u32 = util::to_big_endian(*cipher_u32);
    }
    for (size_t i = 0; i < 16; i+=4) {
      uint32_t* key_u32 = (uint32_t*)(key + i);
      *key_u32 = util::to_big_endian(*key_u32);
    }
    vector<uint32_t> expanded_key = expand_key((uint32_t*)key);
    for (size_t i = 0; i < 16; ++i) {
      auto tmp = expanded_key[i];
      expanded_key[i] = expanded_key[31 - i];
      expanded_key[31 - i] = tmp;
    }
    vector<uint8_t> ret; ret.reserve(len);
    for (size_t i = 0; i < len; i += 16) {
      uint32_t tmp[4];
      encrypt_block((uint32_t*)(cipher + i), expanded_key.data(), tmp);
      for (size_t j = 0; j < 16; ++j) {
        ret.push_back(((uint8_t*)tmp)[j]);
      }
    }
    // to little endian
    for (size_t i = 0; i < ret.size(); i+=4) {
      uint32_t* ret_u32 = (uint32_t*)(ret.data() + i);
      *ret_u32 = util::to_big_endian(*ret_u32);
    }
    return util::pkcs7_unpad(ret.data(), ret.size());
  }

  void debug() {
    uint8_t plain[16] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0xfe, 0xdc, 0xba, 0x98, 0x76, 0x54, 0x32, 0x10};
    uint8_t key[16] =   {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0xfe, 0xdc, 0xba, 0x98, 0x76, 0x54, 0x32, 0x10};

    auto cipher = encrypt_ecb(plain, 16, key);
    
    for (size_t i = 0; i < 16; ++i) {
      printf("%02x ", cipher[i]);
    }
    printf("\n");

    uint8_t key2[16] =   {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0xfe, 0xdc, 0xba, 0x98, 0x76, 0x54, 0x32, 0x10};
    auto plain2 = decrypt_ecb(cipher.data(), cipher.size(), key2);
    for (size_t i = 0; i < 16; ++i) {
      printf("%02x ", plain2[i]);
    }
    printf("\n");
  }

}