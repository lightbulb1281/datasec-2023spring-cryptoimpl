#pragma once

#include <cstdint>
#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <cstring>

namespace util {

  inline void print_binary_uint8(uint8_t value) {
    for (int i = 7; i >= 0; --i) {
      std::cout << ((value >> i) & 1);
    }
  }

  inline void print_binary_uint32(uint32_t value) {
    for (int i = 31; i >= 0; --i) {
      std::cout << ((value >> i) & 1);
    }
  }

  inline void print_binary_uint64(uint64_t value) {
    for (int i = 63; i >= 0; --i) {
      std::cout << ((value >> i) & 1);
    }
  }

  inline void print_hex_uint32(uint32_t value) {
    std::cout << std::setw(8) << std::setfill('0') << std::hex << value;
  }

  inline void print_hex_uint64(uint64_t value) {
    std::cout << std::setw(16) << std::setfill('0') << std::hex << value;
  }

  void print_hex_uint32_array(const uint32_t* value, int size);

  void print_hex_uint64_array(const uint64_t* value, int size);

  void print_hex_uint8_array(const uint8_t* value, int size);

  inline uint32_t to_big_endian(uint32_t value) {
    return ((value & 0x000000FF) << 24) |
      ((value & 0x0000FF00) << 8) |
      ((value & 0x00FF0000) >> 8) |
      ((value & 0xFF000000) >> 24);
  }

  inline uint64_t to_big_endian(uint64_t value) {
    return ((value & 0x00000000000000FF) << 56) |
      ((value & 0x000000000000FF00) << 40) |
      ((value & 0x0000000000FF0000) << 24) |
      ((value & 0x00000000FF000000) << 8) |
      ((value & 0x000000FF00000000) >> 8) |
      ((value & 0x0000FF0000000000) >> 24) |
      ((value & 0x00FF000000000000) >> 40) |
      ((value & 0xFF00000000000000) >> 56);
  }

  inline std::vector<uint8_t> pkcs7_pad(const uint8_t* data, size_t len, size_t block_size) {
    size_t pad_len = block_size - (len % block_size);
    std::vector<uint8_t> padded_data(len + pad_len);
    std::memcpy(padded_data.data(), data, len);
    std::memset(padded_data.data() + len, pad_len, pad_len);
    return padded_data;
  }

  inline std::vector<uint8_t> pkcs7_unpad(const uint8_t* data, size_t len) {
    size_t pad_len = data[len - 1];
    std::vector<uint8_t> unpadded_data(len - pad_len);
    std::memcpy(unpadded_data.data(), data, len - pad_len);
    return unpadded_data;
  }

}