#pragma once

#include "utils.h"
#include <vector>

namespace sha256 {

  const uint32_t k[64] = {
    0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
    0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
    0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
    0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
    0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
    0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
    0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
    0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
  };

  inline uint32_t ch(uint32_t x, uint32_t y, uint32_t z) {
    return (x & y) ^ (~x & z);
  }

  inline uint32_t maj(uint32_t x, uint32_t y, uint32_t z) {
    return (x & y) ^ (x & z) ^ (y & z);
  }

  inline uint32_t rotr(uint32_t x, uint32_t n) {
    return (x >> n) | (x << (32 - n));
  }

  inline uint32_t shr(uint32_t x, uint32_t n) {
    return x >> n;
  }
  
  inline uint32_t rotl(uint32_t x, uint32_t n) {
    return (x << n) | (x >> (32 - n));
  }

  inline uint32_t Sigma0(uint32_t x) {
    return rotr(x, 2) ^ rotr(x, 13) ^ rotr(x, 22);
  }

  inline uint32_t Sigma1(uint32_t x) {
    return rotr(x, 6) ^ rotr(x, 11) ^ rotr(x, 25);
  }

  inline uint32_t sigma0(uint32_t x) {
    return rotr(x, 7) ^ rotr(x, 18) ^ shr(x, 3);
  }

  inline uint32_t sigma1(uint32_t x) {
    return rotr(x, 17) ^ rotr(x, 19) ^ shr(x, 10);
  }

  std::vector<uint32_t> pad(const uint8_t* message, size_t len) {
    std::vector<uint32_t> padded_message;
    padded_message.reserve(len / 4 + 2);
    for (size_t i = 0; i < len; i += 4) {
      uint32_t word = 0;
      for (size_t j = 0; j < 4; ++j) {
        size_t index = i + j;
        uint8_t byte = index < len ? message[index] : (index == len ? 0x80 : 0);
        word |= byte << (8 * (4 - j - 1)); // big endian
      }
      padded_message.push_back(word);
    }
    if (len % 4 == 0) {
      padded_message.push_back(0x80000000);
    }
    while (padded_message.size() % 16 != 14) {
      padded_message.push_back(0);
    }
    uint64_t bit_length = len * 8;
    padded_message.push_back(bit_length >> 32);
    padded_message.push_back(bit_length & 0xFFFFFFFF);
    // util::print_hex_uint32_array(padded_message.data(), padded_message.size());
    return padded_message;
  }

  std::vector<uint32_t> digest(const uint8_t* message, size_t len) {
    std::vector<uint32_t> padded_message = pad(message, len);
    std::vector<uint32_t> h(8);
    h[0] = 0x6a09e667;
    h[1] = 0xbb67ae85;
    h[2] = 0x3c6ef372;
    h[3] = 0xa54ff53a;
    h[4] = 0x510e527f;
    h[5] = 0x9b05688c;
    h[6] = 0x1f83d9ab;
    h[7] = 0x5be0cd19;
    for (size_t i = 0; i < padded_message.size(); i += 16) {
      std::vector<uint32_t> w(64);
      for (size_t j = 0; j < 16; ++j) {
        w[j] = padded_message[i + j];
      }
      for (size_t j = 16; j < 64; ++j) {
        w[j] = sigma1(w[j - 2]) + w[j - 7] + sigma0(w[j - 15]) + w[j - 16];
      }
      std::vector<uint32_t> a(8);
      for (size_t j = 0; j < 8; ++j) {
        a[j] = h[j];
      }
      for (size_t j = 0; j < 64; ++j) {
        uint32_t t1 = a[7] + Sigma1(a[4]) + ch(a[4], a[5], a[6]) + k[j] + w[j];
        uint32_t t2 = Sigma0(a[0]) + maj(a[0], a[1], a[2]);
        for (size_t k = 7; k > 0; --k) {
          a[k] = a[k - 1];
        }
        a[4] += t1;
        a[0] = t1 + t2;
      }
      for (size_t j = 0; j < 8; ++j) {
        h[j] += a[j];
      }
    }
    return h;
  }

  void print_result(const std::vector<uint32_t> digest) {
    // use putchar to put uint8_ts to stdout
    for (size_t i = 0; i < digest.size(); ++i) {
      for (size_t j = 0; j < 4; ++j) {
        putchar((digest[i] >> (8 * (4 - j - 1))) & 0xFF); // big endian
      }
    }
  }

}