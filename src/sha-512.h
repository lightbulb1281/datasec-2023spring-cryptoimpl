#pragma once

#include "utils.h"
#include <vector>

namespace sha512 {

  const uint64_t k[80] = {
    0x428a2f98d728ae22ull, 0x7137449123ef65cdull, 0xb5c0fbcfec4d3b2full, 0xe9b5dba58189dbbcull,
    0x3956c25bf348b538ull, 0x59f111f1b605d019ull, 0x923f82a4af194f9bull, 0xab1c5ed5da6d8118ull,
    0xd807aa98a3030242ull, 0x12835b0145706fbeull, 0x243185be4ee4b28cull, 0x550c7dc3d5ffb4e2ull,
    0x72be5d74f27b896full, 0x80deb1fe3b1696b1ull, 0x9bdc06a725c71235ull, 0xc19bf174cf692694ull,
    0xe49b69c19ef14ad2ull, 0xefbe4786384f25e3ull, 0x0fc19dc68b8cd5b5ull, 0x240ca1cc77ac9c65ull,
    0x2de92c6f592b0275ull, 0x4a7484aa6ea6e483ull, 0x5cb0a9dcbd41fbd4ull, 0x76f988da831153b5ull,
    0x983e5152ee66dfabull, 0xa831c66d2db43210ull, 0xb00327c898fb213full, 0xbf597fc7beef0ee4ull,
    0xc6e00bf33da88fc2ull, 0xd5a79147930aa725ull, 0x06ca6351e003826full, 0x142929670a0e6e70ull,
    0x27b70a8546d22ffcull, 0x2e1b21385c26c926ull, 0x4d2c6dfc5ac42aedull, 0x53380d139d95b3dfull,
    0x650a73548baf63deull, 0x766a0abb3c77b2a8ull, 0x81c2c92e47edaee6ull, 0x92722c851482353bull,
    0xa2bfe8a14cf10364ull, 0xa81a664bbc423001ull, 0xc24b8b70d0f89791ull, 0xc76c51a30654be30ull,
    0xd192e819d6ef5218ull, 0xd69906245565a910ull, 0xf40e35855771202aull, 0x106aa07032bbd1b8ull,
    0x19a4c116b8d2d0c8ull, 0x1e376c085141ab53ull, 0x2748774cdf8eeb99ull, 0x34b0bcb5e19b48a8ull,
    0x391c0cb3c5c95a63ull, 0x4ed8aa4ae3418acbull, 0x5b9cca4f7763e373ull, 0x682e6ff3d6b2b8a3ull,
    0x748f82ee5defb2fcull, 0x78a5636f43172f60ull, 0x84c87814a1f0ab72ull, 0x8cc702081a6439ecull,
    0x90befffa23631e28ull, 0xa4506cebde82bde9ull, 0xbef9a3f7b2c67915ull, 0xc67178f2e372532bull,
    0xca273eceea26619cull, 0xd186b8c721c0c207ull, 0xeada7dd6cde0eb1eull, 0xf57d4f7fee6ed178ull,
    0x06f067aa72176fbaull, 0x0a637dc5a2c898a6ull, 0x113f9804bef90daeull, 0x1b710b35131c471bull,
    0x28db77f523047d84ull, 0x32caab7b40c72493ull, 0x3c9ebe0a15c9bebcull, 0x431d67c49c100d4cull,
    0x4cc5d4becb3e42b6ull, 0x597f299cfc657e2aull, 0x5fcb6fab3ad6faecull, 0x6c44198c4a475817ull
  };

  inline uint64_t ch(uint64_t x, uint64_t y, uint64_t z) {
    return (x & y) ^ (~x & z);
  }

  inline uint64_t maj(uint64_t x, uint64_t y, uint64_t z) {
    return (x & y) ^ (x & z) ^ (y & z);
  }

  inline uint64_t rotr(uint64_t x, uint64_t n) {
    return (x >> n) | (x << (64 - n));
  }

  inline uint64_t shr(uint64_t x, uint64_t n) {
    return x >> n;
  }

  inline uint64_t rotl(uint64_t x, uint64_t n) {
    return (x << n) | (x >> (64 - n));
  }

  inline uint64_t Sigma0(uint64_t x) {
    return rotr(x, 28) ^ rotr(x, 34) ^ rotr(x, 39);
  }

  inline uint64_t Sigma1(uint64_t x) {
    return rotr(x, 14) ^ rotr(x, 18) ^ rotr(x, 41);
  }

  inline uint64_t sigma0(uint64_t x) {
    return rotr(x, 1) ^ rotr(x, 8) ^ shr(x, 7);
  }

  inline uint64_t sigma1(uint64_t x) {
    return rotr(x, 19) ^ rotr(x, 61) ^ shr(x, 6);
  }

  std::vector<uint64_t> pad(const uint8_t* message, size_t len) {
    std::vector<uint64_t> padded_message;
    padded_message.reserve(len / 8 + 1 + 16);
    for (size_t i = 0; i < len; i += 8) {
      uint64_t chunk = 0;
      for (size_t j = 0; j < 8; j++) {
        size_t index = i + j;
        uint8_t byte = index < len ? message[index] : (index == len ? 0x80 : 0);
        chunk |= (uint64_t)(byte) << (8 * (8 - j - 1)); // big endian
      }
      padded_message.push_back(chunk);
    }
    if (len % 8 == 0) {
      padded_message.push_back(0x8000000000000000ull);
    }
    while (padded_message.size() % 16 != 14) {
      padded_message.push_back(0);
    }
    padded_message.push_back(0);
    padded_message.push_back(len * 8);
    return padded_message;
  }

  std::vector<uint64_t> digest(const uint8_t* message, size_t len) {
    std::vector<uint64_t> padded_message = pad(message, len);
    std::vector<uint64_t> h = {
      0x6a09e667f3bcc908ull, 0xbb67ae8584caa73bull, 
      0x3c6ef372fe94f82bull, 0xa54ff53a5f1d36f1ull,
      0x510e527fade682d1ull, 0x9b05688c2b3e6c1full, 
      0x1f83d9abfb41bd6bull, 0x5be0cd19137e2179ull
    };
    for (size_t i = 0; i < padded_message.size() / 16; i++) {
      std::vector<uint64_t> w(80);
      for (size_t j = 0; j < 16; j++) {
        w[j] = padded_message[i * 16 + j];
      }
      for (size_t j = 16; j < 80; j++) {
        w[j] = sigma1(w[j - 2]) + w[j - 7] + sigma0(w[j - 15]) + w[j - 16];
      }
      std::vector<uint64_t> a = h;
      for (size_t j = 0; j < 80; j++) {
        uint64_t t1 = a[7] + Sigma1(a[4]) + ch(a[4], a[5], a[6]) + k[j] + w[j];
        uint64_t t2 = Sigma0(a[0]) + maj(a[0], a[1], a[2]);
        a[7] = a[6];
        a[6] = a[5];
        a[5] = a[4];
        a[4] = a[3] + t1;
        a[3] = a[2];
        a[2] = a[1];
        a[1] = a[0];
        a[0] = t1 + t2;
      }
      for (size_t j = 0; j < 8; j++) {
        h[j] += a[j];
      }
    }
    return h;
  }
  
  void print_result(const std::vector<uint64_t> digest) {
    // use putchar to put uint8_ts to stdout
    for (size_t i = 0; i < digest.size(); i++) {
      for (size_t j = 0; j < 8; j++) {
        putchar((digest[i] >> (8 * (8 - j - 1))) & 0xff); // big endian
      }
    }
  }
}