#pragma once

#include <vector>
#include <cstring>

#include "utils.h"

namespace sha3 {

  typedef uint64_t State[25]; // 1600 bits


  void print_state(const State& A) {
    for (size_t i = 0; i < 25; ++i) {
      if (i % 2 == 0) printf("<");
      // print little endian u8s 
      for (size_t j = 0; j < 8; ++j) {
        printf("%02x", (uint8_t)(A[i] >> (8 * j)));
        if (j != 7) printf(" ");
        else if (i % 2 == 0) printf(" ");
      }
      if (i % 2 == 1) printf(">\n");
    }
    printf(">\n");
  }

  inline uint64_t rotl(uint64_t x, int n) {
    return (x << n) | (x >> (64 - n));
  }

  inline uint64_t rotr(uint64_t x, int n) {
    return (x >> n) | (x << (64 - n));
  }

  inline void theta(State& A) {
    uint64_t C[5];
    uint64_t D[5];
    for (int i = 0; i < 5; ++i) {
      C[i] = A[i] ^ A[i + 5] ^ A[i + 10] ^ A[i + 15] ^ A[i + 20];
    }
    D[0] = C[4] ^ rotl(C[1], 1);
    D[1] = C[0] ^ rotl(C[2], 1);
    D[2] = C[1] ^ rotl(C[3], 1);
    D[3] = C[2] ^ rotl(C[4], 1);
    D[4] = C[3] ^ rotl(C[0], 1);
    for (int i = 0; i < 5; ++i) {
      A[i] ^= D[i];
      A[i + 5] ^= D[i];
      A[i + 10] ^= D[i];
      A[i + 15] ^= D[i];
      A[i + 20] ^= D[i];
    }
  }
  
  inline void rho(State& A) {
    A[1] = rotl(A[1], 1);
    A[2] = rotl(A[2], 62);
    A[3] = rotl(A[3], 28);
    A[4] = rotl(A[4], 27);
    A[5] = rotl(A[5], 36);
    A[6] = rotl(A[6], 44);
    A[7] = rotl(A[7], 6);
    A[8] = rotl(A[8], 55);
    A[9] = rotl(A[9], 20);
    A[10] = rotl(A[10], 3);
    A[11] = rotl(A[11], 10);
    A[12] = rotl(A[12], 43);
    A[13] = rotl(A[13], 25);
    A[14] = rotl(A[14], 39);
    A[15] = rotl(A[15], 41);
    A[16] = rotl(A[16], 45);
    A[17] = rotl(A[17], 15);
    A[18] = rotl(A[18], 21);
    A[19] = rotl(A[19], 8);
    A[20] = rotl(A[20], 18);
    A[21] = rotl(A[21], 2);
    A[22] = rotl(A[22], 61);
    A[23] = rotl(A[23], 56);
    A[24] = rotl(A[24], 14);
  }

  inline void pi(const State& A, State& B) {
    B[0]  = A[0];
    B[1]  = A[6];
    B[2]  = A[12];
    B[3]  = A[18];
    B[4]  = A[24];
    B[5]  = A[3];
    B[6]  = A[9];
    B[7]  = A[10];
    B[8]  = A[16];
    B[9]  = A[22];
    B[10] = A[1];
    B[11] = A[7];
    B[12] = A[13];
    B[13] = A[19];
    B[14] = A[20];
    B[15] = A[4];
    B[16] = A[5];
    B[17] = A[11];
    B[18] = A[17];
    B[19] = A[23];
    B[20] = A[2];
    B[21] = A[8];
    B[22] = A[14];
    B[23] = A[15];
    B[24] = A[21];
  }

  inline void chi(const State& A, State& B) {
    for (int i = 0; i < 25; i += 5) {
      B[i] = A[i] ^ ((~A[i + 1]) & A[i + 2]);
      B[i + 1] = A[i + 1] ^ ((~A[i + 2]) & A[i + 3]);
      B[i + 2] = A[i + 2] ^ ((~A[i + 3]) & A[i + 4]);
      B[i + 3] = A[i + 3] ^ ((~A[i + 4]) & A[i]);
      B[i + 4] = A[i + 4] ^ ((~A[i]) & A[i + 1]);
    }
  }

  const uint64_t RC[24] = {
    0x0000000000000001, 0x0000000000008082, 0x800000000000808A,
    0x8000000080008000, 0x000000000000808B, 0x0000000080000001,
    0x8000000080008081, 0x8000000000008009, 0x000000000000008A,
    0x0000000000000088, 0x0000000080008009, 0x000000008000000A,
    0x000000008000808B, 0x800000000000008B, 0x8000000000008089,
    0x8000000000008003, 0x8000000000008002, 0x8000000000000080,
    0x000000000000800A, 0x800000008000000A, 0x8000000080008081,
    0x8000000000008080, 0x0000000080000001, 0x8000000080008008
  };

  inline void iota(State& A, int round) {
    A[0] ^= RC[round];
  }

  inline void round(State& A, int round) {
    State B;
    theta(A);
    rho(A);
    pi(A, B);
    chi(B, A);
    iota(A, round);
  }

  inline void keccakf(State& A) {
    for (int i = 0; i < 24; ++i) {
      round(A, i);
    }
  }
  
  std::vector<uint8_t> pad(const uint8_t* message, size_t len, size_t x) {
    size_t n = len;
    size_t r = x / 8;
    size_t total_uint8s = (n % r == 0) ? n + r : n + r - (n % r);
    // Copy and pad
    std::vector<uint8_t> padded(total_uint8s);
    std::copy(message, message + n, padded.begin());
    // First append 0b01 then 0b10...01
    if (n != total_uint8s - 1) {
      padded[n] = 0x06;
      memset(&padded[n + 1], 0, total_uint8s - n - 2);
      padded[total_uint8s - 1] = 0x80;
    } else {
      padded[n] = 0x86;
    }
    return padded;
  }

  inline uint64_t load64(const uint8_t* x) {
    uint64_t r = 0;
    for (int i = 0; i < 8; ++i) {
      r |= (uint64_t)x[i] << (8 * i);
    }
    return r;
  }

  inline void store64(uint8_t* x, uint64_t u) {
    for (int i = 0; i < 8; ++i) {
      x[i] = (uint8_t)(u >> (8 * i));
    }
  }

  std::vector<uint8_t> keccak(const uint8_t* message, size_t len, size_t r_bits, size_t output_bits) {
    size_t r = r_bits / 8;
    size_t output_len = output_bits / 8;
    std::vector<uint8_t> padded = pad(message, len, r_bits);
    size_t n = padded.size() / r;
    State A;
    memset(A, 0, sizeof(A));
    for (size_t i = 0; i < n; ++i) {
      for (size_t j = 0; j < r / 8; ++j) {
        A[j] ^= load64(&padded[i * r + j * 8]);
      }
      keccakf(A);
    }
    std::vector<uint8_t> result(output_len);
    for (size_t i = 0; i < output_len / 8; ++i) {
      store64(&result[i * 8], A[i]);
    }
    return result;
  }

  std::vector<uint8_t> sha3_256(const uint8_t* message, size_t len) {
    return keccak(message, len, 1088, 256);
  }

  std::vector<uint8_t> sha3_512(const uint8_t* message, size_t len) {
    return keccak(message, len, 576, 512);
  }

  void print_result(const std::vector<uint8_t>& result) {
    for (size_t i = 0; i < result.size(); ++i) {
      putchar(result[i]);
    }
  }

}