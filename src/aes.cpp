#include "aes.h"

namespace aes {

  static bool mul_initialized = false;
  static uint8_t mul2[256] = {};
  static uint8_t mul3[256] = {};
  static uint8_t mul9[256] = {};
  static uint8_t mul11[256] = {};
  static uint8_t mul13[256] = {};
  static uint8_t mul14[256] = {};


  inline constexpr uint8_t mul2f(uint8_t x) {
    // if x*2 > 255, then we need to substract 0b100011011 (irreducible polynomial is x^8 + x^4 + x^3 + x + 1)
    return (x & 0x80) ? ((x << 1) ^ 0x1b) : (x << 1);
  }

  inline constexpr uint8_t mul3f(uint8_t x) {
    return mul2f(x) ^ x;
  }

  inline constexpr uint8_t mul9f(uint8_t x) {
    return mul2f(mul2f(mul2f(x))) ^ x;
  }

  inline constexpr uint8_t mul11f(uint8_t x) {
    return mul2f(mul2f(mul2f(x)) ^ x) ^ x;
  }

  inline constexpr uint8_t mul13f(uint8_t x) {
    return mul2f(mul2f(mul2f(x) ^ x)) ^ x;
  }

  inline constexpr uint8_t mul14f(uint8_t x) {
    return mul2f(mul2f(mul2f(x) ^ x) ^ x);
  }

  void initialize_mul_tables() {
    if (mul_initialized) {
      return;
    }
    for (int i = 0; i < 256; i++) {
      mul2[i] = mul2f(i);
      mul3[i] = mul3f(i);
      mul9[i] = mul9f(i);
      mul11[i] = mul11f(i);
      mul13[i] = mul13f(i);
      mul14[i] = mul14f(i);
    }
    mul_initialized = true;
  }

  
  void mix_columns(State& state) {
    initialize_mul_tables();
    uint8_t tmp[16];
    std::memcpy(tmp, state, 16);
    state[0] = mul2[tmp[0]] ^ mul3[tmp[1]] ^ tmp[2] ^ tmp[3];
    state[1] = tmp[0] ^ mul2[tmp[1]] ^ mul3[tmp[2]] ^ tmp[3];
    state[2] = tmp[0] ^ tmp[1] ^ mul2[tmp[2]] ^ mul3[tmp[3]];
    state[3] = mul3[tmp[0]] ^ tmp[1] ^ tmp[2] ^ mul2[tmp[3]];
    state[4] = mul2[tmp[4]] ^ mul3[tmp[5]] ^ tmp[6] ^ tmp[7];
    state[5] = tmp[4] ^ mul2[tmp[5]] ^ mul3[tmp[6]] ^ tmp[7];
    state[6] = tmp[4] ^ tmp[5] ^ mul2[tmp[6]] ^ mul3[tmp[7]];
    state[7] = mul3[tmp[4]] ^ tmp[5] ^ tmp[6] ^ mul2[tmp[7]];
    state[8] = mul2[tmp[8]] ^ mul3[tmp[9]] ^ tmp[10] ^ tmp[11];
    state[9] = tmp[8] ^ mul2[tmp[9]] ^ mul3[tmp[10]] ^ tmp[11];
    state[10] = tmp[8] ^ tmp[9] ^ mul2[tmp[10]] ^ mul3[tmp[11]];
    state[11] = mul3[tmp[8]] ^ tmp[9] ^ tmp[10] ^ mul2[tmp[11]];
    state[12] = mul2[tmp[12]] ^ mul3[tmp[13]] ^ tmp[14] ^ tmp[15];
    state[13] = tmp[12] ^ mul2[tmp[13]] ^ mul3[tmp[14]] ^ tmp[15];
    state[14] = tmp[12] ^ tmp[13] ^ mul2[tmp[14]] ^ mul3[tmp[15]];
    state[15] = mul3[tmp[12]] ^ tmp[13] ^ tmp[14] ^ mul2[tmp[15]];
  }

  void inv_mix_columns(State& state) {
    initialize_mul_tables();
    uint8_t tmp[16];
    std::memcpy(tmp, state, 16);
    state[0] = mul14[tmp[0]] ^ mul11[tmp[1]] ^ mul13[tmp[2]] ^ mul9[tmp[3]];
    state[1] = mul9[tmp[0]] ^ mul14[tmp[1]] ^ mul11[tmp[2]] ^ mul13[tmp[3]];
    state[2] = mul13[tmp[0]] ^ mul9[tmp[1]] ^ mul14[tmp[2]] ^ mul11[tmp[3]];
    state[3] = mul11[tmp[0]] ^ mul13[tmp[1]] ^ mul9[tmp[2]] ^ mul14[tmp[3]];
    state[4] = mul14[tmp[4]] ^ mul11[tmp[5]] ^ mul13[tmp[6]] ^ mul9[tmp[7]];
    state[5] = mul9[tmp[4]] ^ mul14[tmp[5]] ^ mul11[tmp[6]] ^ mul13[tmp[7]];
    state[6] = mul13[tmp[4]] ^ mul9[tmp[5]] ^ mul14[tmp[6]] ^ mul11[tmp[7]];
    state[7] = mul11[tmp[4]] ^ mul13[tmp[5]] ^ mul9[tmp[6]] ^ mul14[tmp[7]];
    state[8] = mul14[tmp[8]] ^ mul11[tmp[9]] ^ mul13[tmp[10]] ^ mul9[tmp[11]];
    state[9] = mul9[tmp[8]] ^ mul14[tmp[9]] ^ mul11[tmp[10]] ^ mul13[tmp[11]];
    state[10] = mul13[tmp[8]] ^ mul9[tmp[9]] ^ mul14[tmp[10]] ^ mul11[tmp[11]];
    state[11] = mul11[tmp[8]] ^ mul13[tmp[9]] ^ mul9[tmp[10]] ^ mul14[tmp[11]];
    state[12] = mul14[tmp[12]] ^ mul11[tmp[13]] ^ mul13[tmp[14]] ^ mul9[tmp[15]];
    state[13] = mul9[tmp[12]] ^ mul14[tmp[13]] ^ mul11[tmp[14]] ^ mul13[tmp[15]];
    state[14] = mul13[tmp[12]] ^ mul9[tmp[13]] ^ mul14[tmp[14]] ^ mul11[tmp[15]];
    state[15] = mul11[tmp[12]] ^ mul13[tmp[13]] ^ mul9[tmp[14]] ^ mul14[tmp[15]];
  }

}