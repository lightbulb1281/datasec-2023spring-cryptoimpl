## Compiling

```sh
mkdir build
cd build
cmake ..
make
cd ..
```

## Testing

```sh
# Correct test
pip install pycryptodame gmssl # install requirements
python3 test/test.py

# Time test
cd build
./timetest
```