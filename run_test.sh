# Compile
mkdir -p build
cd build
cmake ..
make
cd ..

# Run tests
python3 test/test.py