#include <string>
#include <vector>
#include <sys/time.h>
#include <cassert>
#include <map>
#include <iostream>
#include <iomanip>
#include <stdint.h>
#include "../src/aes.h"
#include "../src/sm3.h"
#include "../src/sha-256.h"
#include "../src/sha-512.h"
#include "../src/sha3.h"
#include "../src/sm4.h"

class Timer {
public:
    std::vector<timeval> times;
    std::vector<double> accumulated; // ms
    std::vector<std::string> names;
    Timer() {}
    long registerTimer(std::string name = "") {
        times.push_back(timeval()); 
        accumulated.push_back(0);
        int ret = times.size() - 1;
        names.push_back(name);
        return ret;
    }
    void tick(long i = 0) {
        if (times.size() < 1) registerTimer();
        assert(i < times.size());
        gettimeofday(&times[i], 0);
    }
    double tock(long i = 0) {
        assert(i < times.size());
        timeval s; gettimeofday(&s, 0);
        auto timeElapsed = (s.tv_sec - times[i].tv_sec) * 1000.0;
        timeElapsed += (s.tv_usec - times[i].tv_usec) / 1000.0;
        accumulated[i] += timeElapsed;
        return accumulated[i];
    }
    
    void clear() {
        times.clear();
        accumulated.clear();
        names.clear();
    }

    std::map<std::string, double> gather(double divisor = 1) {
        std::map<std::string, double> p;
        for (long i=0; i<times.size(); i++) {
            p[names[i]] = accumulated[i] / divisor;
        }
        clear();
        return p;
    }
};


void printTimer(std::map<std::string, double> r) {
    for (auto& p: r) {
        std::cout << std::setw(25) << std::right << p.first << ":";
        std::cout << std::setw(10) << std::right << std::fixed << std::setprecision(3)
            << p.second << " ms" << std::endl;
    }
}

void test(size_t data_length, size_t repeat = 100) {
    std::vector<uint8_t> data(data_length);
    for (size_t i = 0; i<data.size(); i++) {
        data[i] = i % 256;
    }
    Timer t; long i;

    // AES

    std::vector<uint8_t> key128(16);
    std::vector<uint8_t> cipher;
    for (size_t i = 0; i < key128.size(); i++) {
        key128[i] = i % 256;
    }
    i = t.registerTimer("aes128-ecb-enc");
    t.tick(i);
    for (size_t t = 0; t < repeat; t++) {
        cipher = aes::aes128_encrypt_ecb(data.data(), data.size(), key128.data());
    }
    t.tock(i);
    i = t.registerTimer("aes128-ecb-dec");
    t.tick(i);
    for (size_t t = 0; t < repeat; t++) {
        aes::aes128_decrypt_ecb(cipher.data(), cipher.size(), key128.data());
    }
    t.tock(i);
    printTimer(t.gather(repeat));

    std::vector<uint8_t> key192(24);
    for (size_t i = 0; i < key192.size(); i++) {
        key192[i] = i % 256;
    }
    i = t.registerTimer("aes192-ecb-enc");
    t.tick(i);
    for (size_t t = 0; t < repeat; t++) {
        cipher = aes::aes192_encrypt_ecb(data.data(), data.size(), key192.data());
    }
    t.tock(i);
    i = t.registerTimer("aes192-ecb-dec");
    t.tick(i);
    for (size_t t = 0; t < repeat; t++) {
        aes::aes192_decrypt_ecb(cipher.data(), cipher.size(), key192.data());
    }
    t.tock(i);
    printTimer(t.gather(repeat));

    std::vector<uint8_t> key256(32);
    for (size_t i = 0; i < key256.size(); i++) {
        key256[i] = i % 256;
    }
    i = t.registerTimer("aes256-ecb-enc");
    t.tick(i);
    for (size_t t = 0; t < repeat; t++) {
        cipher = aes::aes256_encrypt_ecb(data.data(), data.size(), key256.data());
    }
    t.tock(i);
    i = t.registerTimer("aes256-ecb-dec");
    t.tick(i);
    for (size_t t = 0; t < repeat; t++) {
        aes::aes256_decrypt_ecb(cipher.data(), cipher.size(), key256.data());
    }
    t.tock(i);
    printTimer(t.gather(repeat));


    // SM4

    for (size_t i = 0; i < key128.size(); i++) {
        key128[i] = i % 256;
    }
    i = t.registerTimer("sm4-ecb-enc");
    t.tick(i);
    for (size_t t = 0; t < repeat; t++) {
        cipher = sm4::encrypt_ecb(data.data(), data.size(), key128.data());
    }
    t.tock(i);
    i = t.registerTimer("sm4-ecb-dec");
    t.tick(i);
    for (size_t t = 0; t < repeat; t++) {
        sm4::decrypt_ecb(cipher.data(), cipher.size(), key128.data());
    }
    t.tock(i);
    printTimer(t.gather(repeat));

    // sha-2

    i = t.registerTimer("sha-256");
    t.tick(i);
    for (size_t t = 0; t < repeat; t++) {
        sha256::digest(data.data(), data.size());
    }
    t.tock(i);

    i = t.registerTimer("sha-512");
    t.tick(i);
    for (size_t t = 0; t < repeat; t++) {
        sha512::digest(data.data(), data.size());
    }
    t.tock(i);
    printTimer(t.gather(repeat));


    // sha-3

    i = t.registerTimer("sha3-256");
    t.tick(i);
    for (size_t t = 0; t < repeat; t++) {
        sha3::sha3_256(data.data(), data.size());
    }
    t.tock(i);

    i = t.registerTimer("sha3-512");
    t.tick(i);
    for (size_t t = 0; t < repeat; t++) {
        sha3::sha3_512(data.data(), data.size());
    }
    t.tock(i);
    printTimer(t.gather(repeat));

    // sm3

    i = t.registerTimer("sm3");
    t.tick(i);
    for (size_t t = 0; t < repeat; t++) {
        sm3::digest(data.data(), data.size());
    }
    t.tock(i);
    printTimer(t.gather(repeat));
}

int main() {
    printf("[16KB data]\n");
    test(16 * 1024); // 16KB
    printf("[4MB data]\n");
    test(4 * 1024 * 1024, 4); // 4MB
    return 0;
}