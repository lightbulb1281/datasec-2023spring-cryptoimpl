import subprocess
import os
import hashlib
import Crypto.Cipher.AES
import Crypto.Util.Padding
from gmssl import sm3, sm4

def random_bytes(len: int):
  return os.urandom(len)

def get_description(input_bytes):
  if len(input_bytes) == 0:
    input_describe = "(Empty input)"
  elif isinstance(input_bytes, str): 
    input_describe = input_bytes
  elif len(input_bytes) < 16: 
    input_describe = "0x" + input_bytes.hex()
  else: 
    input_describe = "0x" + input_bytes[:16].hex() + "..." + f" ({len(input_bytes)} bytes)"
  return input_describe

def expect(algo, input_bytes, output_bytes: bytes, key_bytes = None):
  input_describe = get_description(input_bytes)
  if isinstance(input_bytes, str):
    input_bytes = input_bytes.encode()

  f = open("build/input.txt", "wb")
  if key_bytes is not None:
    f.write(key_bytes)
  f.write(input_bytes)
  f.close()

  subprocess.run(["build/main", algo], stdin=open("build/input.txt", "rb"), stdout=open("build/output.txt", "wb"))
  f = open("build/output.txt", "rb")
  result = f.read()
  f.close()
  
  if result != output_bytes:
    output_describe = get_description(output_bytes)
    result_describe = get_description(result)
    print(f"[FAILED] [{algo}] " + input_describe)
    print(f"    expected: {output_describe}")
    print(f"    got:      {result_describe}")
  else:
    print(f"[PASSED] [{algo}] " + input_describe)

def test_sha256(s):
  hasher = hashlib.sha256()
  hasher.update(s.encode() if isinstance(s, str) else s)
  expect("sha256", s, hasher.digest())

def test_sha512(s):
  hasher = hashlib.sha512()
  hasher.update(s.encode() if isinstance(s, str) else s)
  expect("sha512", s, hasher.digest())

def test_sha3_256(s):
  hasher = hashlib.sha3_256()
  hasher.update(s.encode() if isinstance(s, str) else s)
  expect("sha3-256", s, hasher.digest())

def test_sha3_512(s):
  hasher = hashlib.sha3_512()
  hasher.update(s.encode() if isinstance(s, str) else s)
  expect("sha3-512", s, hasher.digest())

def test_aes128(s):
  key = random_bytes(16)
  aes = Crypto.Cipher.AES.new(key, Crypto.Cipher.AES.MODE_ECB)
  message = s.encode() if isinstance(s, str) else s
  message_padded = Crypto.Util.Padding.pad(message, 16)
  encrypted = aes.encrypt(message_padded)
  expect("aes128-ecb-enc", s, encrypted, key)
  expect("aes128-ecb-dec", encrypted, message, key)

def test_aes192(s):
  key = random_bytes(192 // 8)
  aes = Crypto.Cipher.AES.new(key, Crypto.Cipher.AES.MODE_ECB)
  message = s.encode() if isinstance(s, str) else s
  message_padded = Crypto.Util.Padding.pad(message, 16)
  encrypted = aes.encrypt(message_padded)
  expect("aes192-ecb-enc", s, encrypted, key)
  expect("aes192-ecb-dec", encrypted, message, key)

def test_aes256(s):
  key = random_bytes(256 // 8)
  aes = Crypto.Cipher.AES.new(key, Crypto.Cipher.AES.MODE_ECB)
  message = s.encode() if isinstance(s, str) else s
  message_padded = Crypto.Util.Padding.pad(message, 16)
  encrypted = aes.encrypt(message_padded)
  expect("aes256-ecb-enc", s, encrypted, key)
  expect("aes256-ecb-dec", encrypted, message, key)

def test_sm3(s):
  h = sm3.sm3_hash(list(s.encode() if isinstance(s, str) else s))
  h_bytes = []
  for i in range(0, len(h)//2):
    h_bytes.append(int(h[i*2:i*2+2], 16))
  h_bytes = bytes(h_bytes)
  expect("sm3", s, h_bytes)

def test_sm4(s):
  key = random_bytes(16)
  h = sm4.CryptSM4()
  h.set_key(key, sm4.SM4_ENCRYPT)
  message = s.encode() if isinstance(s, str) else s
  encrypted = h.crypt_ecb(message)
  expect("sm4-ecb-enc", s, encrypted, key)
  expect("sm4-ecb-dec", encrypted, message, key)


def test_all():
  test_strings = [
    "",
    "abc",
    "hello world!",
    "八百标兵奔北坡。",
    random_bytes(12345),
  ]

  for s in test_strings: test_sha256(s)
  for s in test_strings: test_sha512(s)
  for s in test_strings: test_sha3_256(s)
  for s in test_strings: test_sha3_512(s)
  for s in test_strings: test_aes128(s)
  for s in test_strings: test_aes192(s)
  for s in test_strings: test_aes256(s)
  for s in test_strings: test_sm3(s)
  for s in test_strings: test_sm4(s)

if __name__ == "__main__":
  test_all()
